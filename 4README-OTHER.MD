dockerMySql
If u dont have docker installed in centos then use below step for installation

yum install -y http://mirror.centos.org/centos/7/extras/x86_64/Packages/container-selinux-2.42-1.gitad8f0f7.el7.noarch.rpm

curl -fsSL https://get.docker.com/ | sh

Or

yum install docker

systemctl start docker

#########For Mysql Setup############################################

1) Forward Iptables

sysctl net.ipv4.ip_forward=1

2)Check Docker Version

docker -v
Docker version 18.03.0-ce, build 0520e24

3)Pull a mysql docker Image

#docker pull mysql/mysql-server

4)Run the mysql container

#docker run --name=mysql -d mysql/mysql-server

5)check running container

#docker ps

6)check mysql logs

#docker logs mysql

7)check root password of mysql for first time use

#docker logs mysql 2>&1 | grep GENERATED

8)Login into mysql

#docker exec -it mysql mysql -uroot -p

mysql>ALTER USER 'root'@'localhost' IDENTIFIED BY 'newpassword';
mysql>CREATE USER 'test'@'%' IDENTIFIED BY 'newpassword';
mysql>GRANT ALL PRIVILEGES ON *.* TO 'test'@'%';
mysql>ALTER USER 'test'@'%' IDENTIFIED WITH mysql_native_password BY 'newpassword';
mysql>flush privileges;
mysql>EXIT

9)Check ip address of Mysql container

#docker inspect mysql | grep -w 'IPAddress' | head -1

10)login in mysql with ip address & new User

#mysql -utest -pnewpassword -hIPADDRESS

mysql -utest -pnewpassword -h172.17.0.1
