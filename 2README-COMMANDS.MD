dockerMySql is a Docker instance of MySql that is for EJ's Database

Should be able to deploy on
    MAC
    Ubuntu
    CentOS or RedHat
    Windows

**How to login to a shell**

sudo docker exec -it mysql /bin/bash

**How to get the ip address**

sudo docker inspect mysql | grep -w 'IPAddress' | head -1

**Login to mysql container**

sudo docker exec -it mysql mysql -uroot -pnewpassword

**change password on database**

sudo docker run --name some-mysql -e MYSQL_ROOT_PASSWORD=newpassword -d mysql


**Other commands**

docker images

docker exec -it 54243b35e411 /bin/bash

docker ps
docker stop 54243b35e411
docker delete 54243b35e411

docker --help

docker kill 54243b35e411

docker rm 54243b35e411

sudo docker logs mysql 2>&1 | grep GENERATED

docker tag mysql-20181020-1  docker.io/ejbest/mysql-20181020-1

docker push docker.io/ejbest/mysql-20181020-1

mystring="88888888"

docker login -u ejbest -p "$mystring"

docker push docker.io/ejbest/mysql-20181020-1

:-)
