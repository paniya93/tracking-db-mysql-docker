#!/bin/bash
 
################################################################
##
##   MySQL Database Backup Script 
##   Last Update: Aug 23, 2021
##
################################################################
 
export PATH=/bin:/usr/bin:/usr/local/bin
TODAY=`date +"%Y-%m-%d-%H-%M-%S"`
TODAY=`date +"%Y-%m-%d-%H-%M-%S"`
 
################################################################
################## Update below values  ########################
 
DB_BACKUP_PATH='./dbbackup'
MYSQL_HOST='147.182.130.117'
MYSQL_PORT='3306'
MYSQL_USER='sa'
MYSQL_PASSWORD='newpassword'
DATABASE_NAME='ejs'
BACKUP_RETAIN_DAYS=30   ## Number of days to keep local backup copy
 
#################################################################
 
mkdir -p ${DB_BACKUP_PATH}/${TODAY}
echo "Backup started for database - ${DATABASE_NAME}"
 
 
mysqldump -h ${MYSQL_HOST} \
   -P ${MYSQL_PORT} \
   -u ${MYSQL_USER} \
   -p${MYSQL_PASSWORD} \
   ${DATABASE_NAME} | gzip > ${DB_BACKUP_PATH}/${TODAY}/ejs-db-backup-${TODAY}.sql.gz
 
if [ $? -eq 0 ]; then
  echo "Database backup successfully completed"
else
  echo "Error found during backup"
  exit 1
fi
 
 
##### Remove backups older than {BACKUP_RETAIN_DAYS} days  #####
 
# DBDELDATE=`date +"%d%b%Y" --date="${BACKUP_RETAIN_DAYS} days ago"`
 
# if [ ! -z ${DB_BACKUP_PATH} ]; then
#       cd ${DB_BACKUP_PATH}
#       if [ ! -z ${DBDELDATE} ] && [ -d ${DBDELDATE} ]; then
#             rm -rf ${DBDELDATE}
#       fi
# fi
 
### End of script ####