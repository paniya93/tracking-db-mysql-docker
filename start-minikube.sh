# https://github.com/kubernetes/minikube/issues/10310 
minikube delete --all --purge 
docker ps | grep minikube 
chmod +x minikube-darwin-arm64
mv minikube /usr/local/bin
minikube start
kubectl version
minikube dashboard

# https://kubernetes.io/docs/tasks/access-application-cluster/service-access-application-cluster 
minikube service example --url


