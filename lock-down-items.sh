#!/bin/bash
################################################################
##
##   User Management Script 
##   Last Update: Aug 23, 2021
##
################################################################

DATABASE_NAME='ejs'
MYSQL_HOST='147.182.130.117'
MYSQL_PORT='3306'
MYSQL_USER='sa'
MYSQL_PASSWORD='newpassword'
USER1='read-write1'
USER2='read-write2'
READUSR='readonly'
USER1_PASSWORD='newpassword'
USER2_PASSWORD='newpassword'
READUSR_PASSWORD=
mysql -h ${MYSQL_HOST} \
   -P${MYSQL_PORT} \
   -u${MYSQL_USER} \
   -p${MYSQL_PASSWORD} \
   ${DATABASE_NAME} -e "ALTER USER 'sa'@'localhost' IDENTIFIED BY '123';
      CREATE USER ‘read-write1‘@’localhost’ IDENTIFIED BY ‘read-write1‘;
      GRANT ALL ON database TO read-write1@'localhost' IDENTIFIED BY 'read-write1';
      CREATE USER ‘read-write2‘@’localhost’ IDENTIFIED BY ‘read-write2‘;
      GRANT ALL ON database TO read-write2@'localhost' IDENTIFIED BY 'read-write2';
      CREATE USER ‘readonly‘@’localhost’ IDENTIFIED BY ‘readonly‘;
      GRANT SELECT, SHOW VIEW ON database TO readonly@'localhost' IDENTIFIED BY 'read-write1';
      FLUSH PRIVILEGES;"



